---
title: "Acadêmico"
date: 2022-10-04T22:22:58-03:00
draft: false
---
![image alt text](/images/academico.jpeg)
#### A diretoria acadêmica é responsável por organizar eventos que buscam acrescentar a formação acadêmica, como visitas técnicas, palestras, cursos, manuais e oficinas. Além disso, é por meio dela que a comunicação do estudante com o departamento é feita. Também é representante da Engenharia Mecânica e Mecatrônica perante o Diretório Acadêmico.