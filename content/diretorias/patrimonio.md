---
title: "Patrimonio"
date: 2022-10-02T16:55:03-03:00
draft: false
---
![image alt text](/images/patrimonio.jpeg)
#### É a diretoria responsável por manter o patrimônio físico , cultural e digital, manter a nossa sede limpa e organizada para providenciar o maior conforto possível e atender a demanda dos alunos. No lado cultural, fomentar sempre a cultura na escola politécnica com debates, música, textos diversos por meio d’O Corvo.

