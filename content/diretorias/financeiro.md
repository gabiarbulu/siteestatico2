---
title: "Financeiro"
date: 2022-10-04T22:26:10-03:00
draft: false
---
![image alt text](/images/financeiro.jpeg)
#### A diretoria financeira é responsável pelo controle do caixa do Centro Acadêmico, pela capitação de verbas e pela venda de itens relacionados aos cursos de Mecânica e Mecatrônica. Além disso, viabiliza financeiramente a realização dos diversos projetos da entidade ao longo do ano.