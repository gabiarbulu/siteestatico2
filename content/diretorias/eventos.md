---
title: "Eventos"
date: 2022-10-04T22:22:49-03:00
draft: false
---
![image alt text](/images/eventos.jpeg)
#### É a diretoria que planeja e executa os eventos do CAM, tais como o ChurrasCAM, Recepção ou o CAM Games, promovendo a interação e a integração dos estudantes. Além disso, também organizamos conversas com outras faculdades, ou pessoas/empresas, com temas que agreguem para formação.