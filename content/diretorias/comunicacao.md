---
title: "Comunicação"
date: 2022-10-02T16:53:43-03:00
draft: false
---
![image alt text](/images/comunicacao.jpeg)

#### É a diretoria responsável por manter a comunicação clara e objetiva entre a Gestão do CAM, todos os alunos, a comunidade politécnica e a sociedade como um todo. Fica encarregada de manter as redes sociais do CAM, além de preparar a arte para divulgações e eventos.