---
title: "Social"
date: 2022-10-04T22:24:25-03:00
draft: false
---
![image alt text](/images/social.jpeg)

#### Essa diretoria é responsável por repassar informações de permanência e realiza projetos sociais. Temos tradicionalmente oficinas de brinquedos e carrinhos voltadas às crianças, eventos promovendo saúde mental e conscientização e campanhas de doações. Se você tiver qualquer problema relacionado à permanência ou à saúde mental, os diretores sociais podem te ajudar!