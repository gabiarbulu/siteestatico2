---
title: "Sobre"
date: 2022-10-02T15:24:03-03:00
draft: false
---
O CAM, Centro Acadêmico Mecânica e Mecatrônica, é a entidade representativa dos alunos das Engenharias Mecânica e Mecatrônica da Escola Politécnica da USP. Foi fundado em 1987 com o objetivo de acolher, representar e integrar os alunos desses cursos e, hoje em dia, representa mais de 800 estudantes.

Nosso papel é fazer com que você se sinta bem acolhido nesse novo ambiente que é a Poli! Além disso, representamos alunos da engenharia mecânica e da engenharia mecatrônica na Poli e realizamos várias ações desde semanas acadêmicas, projetos sociais, festas e até mesmo o Integra, a melhor gincana de integração! Também é dever do centro acadêmico representar os alunos de seus cursos frente aos órgãos colegiados, podendo assim levar a voz dos alunos para discussões e alterações dos cursos Nosso espaço físico fica no fundo do prédio da Mecânica, que é uma área multiuso para você relaxar, esperar a próxima aula, conversar com os amigos, dormir em um dos sofás, jogar videogame, tênis de mesa etc. Estaremos sempre de portas abertas (mesmo que figurativamente agora)!
