---
title: "Semov"
date: 2022-10-02T16:50:31-03:00
draft: false
---
![image alt text](/images/semov.png)

#### A Semana de Engenharia de Movimento é um evento acadêmico anual organizado pelo Centro Acadêmico Mecânica e Mecatrônica (CAM Poli USP) que busca complementar a formação dos alunos, promover o contato entre aluno e empresa e preparar os engenheiros para o mercado de trabalho. O evento conta com diferentes atividades ao longo da semana, que aproximam os alunos das novidades dentro do mundo da engenharia.

## Sobre nós

### Missão
#### - A Semov tem o intuito de melhor aproximar o aluno ao mercado de trabalho, além de torná-lo um profissional melhor capacitado
### Visão
#### - Mobilizar pessoas almejando impactar suas trajetórias e carreiras.
### Valores
#### - Conexão, Inovação, Inclusão

### Organização
![image alt text](/images/organizacao.jpeg)


## História
#### De 03 a 07 de Outubro aconteceu a primeira edição da SeAuto, que teve dez palestras e dois minicursos, além de exposições no prédio de Engenharia Mecânica, Mecatrônica e Naval. Contamos com a participação das empresas Steer Recursos Humanos, Fiat, GM, Volkswagen e Continental, além de apoio da diretoria da Escola Politécnica, CEA, AEA, Engenharia Automotiva e ANFAVEA.

#### A organização da semana foi feita pelo CAM e foi uma experiência inovadora e de sucesso que viria a ganhar cada vez mais destaque dentro da Escola Politécnica nos anos seguintes.

 